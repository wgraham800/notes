# This is my own personal reference "cheatsheet"

For all those times when you spend a bunch of time reading the man page and doing all kinds of cool things with a tool, but you don't use it for 6 months so the next time you need to use it you have to look everything up again from scratch.

## Git

```
# Show git history with all changes included
git log -p

# Search git history for a simple pattern
git log --source -S 'pattern'

# Search git history for a regex pattern
git log --source -G 'pattern'

# Filter history by an author
git log --author=Will
```

## PS

If you can install `btop`, do that. It looks cool and it works awesome. If you can't, here's some stuff for alternatives.

```
echo testme
```

## Tar

Create one
`tar -cf <archive-name> <files-to-add>`

Extract a tar to a named directory
```
mkdir <dirname>
tar -xvf <tar.tgz> -C <dirname> --strip-components=1
```