# Alcohol
[Link to video](https://www.youtube.com/watch?v=DkS1pkKpILY)

Heavy drinking (12-24 drinks/week) causes neuro-degeneration

## Low to Moderate Drinking
Even low (7-14 drinks on average per week) chronic intake will disrupt your brain.

## Alcohol Biochemistry
Water-soluble and fat-soluble. They don't attach to receptors and park the car. They directly affect the cells.

There are 3 types of alcohol
- Isopropyl
- Methyl
- Ethyl (Ethanol) 
  - The only kind fit for human consumption, but is still toxic.

Ethanol is toxic, and gets converted into acetaldehyde, which is more toxic. It damages and kills cell indescriminately.

Acetalaldehyde gets converted to acetate, which is used for fuel. Your liver does this conversion, but ultimately can't keep up and cells die.

Even though it creates calories to be burned as fuel, there is no nutritional value to alcohol or acetate. 

The build up of Acetalaldehyde is what makes you feel drunk.

Because it's water & fat-soluble, it gets into your bloodstream and is able to cross the blood-brain-barrier, into your brain. 
This decreases neuron activity (in your brain) and shuts down your prefrontal cortex (which controls top-down inhibition).

Cortisol (stress hormone) is also released at a higher level when not drinking. This causes people to "crave" drinks to feel less stressed, less anxiety, etc.

This is true in both the short & long term. 

## Genetics










Eating after drinking does not help to sober up, however, eating before or during drinking will slow absorption of alcohol into the bloodstream.